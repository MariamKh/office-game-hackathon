package com.rockbitegames.office;

public interface SocketListener {
    void onInit(int playerIndex);

    void onPartnerCoordsReceived(NetworkManager.PlayerData playerData);

    void onHit(NetworkManager.DamageData damageData);
}
