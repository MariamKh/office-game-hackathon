package com.rockbitegames.office;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.esotericsoftware.spine.AnimationState;
import com.esotericsoftware.spine.AnimationStateData;
import com.esotericsoftware.spine.Skeleton;
import com.esotericsoftware.spine.SkeletonData;
import com.esotericsoftware.spine.SkeletonJson;
import com.esotericsoftware.spine.SkeletonMeshRenderer;
import com.esotericsoftware.spine.Slot;
import com.esotericsoftware.spine.attachments.AtlasAttachmentLoader;
import com.esotericsoftware.spine.attachments.Attachment;
import com.esotericsoftware.spine.attachments.BoundingBoxAttachment;

class SpineItem {

	private boolean local;
	private Skeleton skeleton;
	private SkeletonMeshRenderer skeletonMeshRenderer;
	private AnimationState state;
	boolean isWalking;
	private Player.Side side;
	private BoundingBoxAttachment headBoundingBox;
	private Slot headSlot;
	private float[] headWorldVertices;
	private BoundingBoxAttachment weaponBoundingBox;
	private Slot weaponSlot;
	private float[] weaponWorldVertices;
	private int[] directions = new int[] {1, -1};
	private Vector2 currSpeed;
	private float currTimeToChange;
	private float tempTime;
	Vector2 currDirection;
	private Rectangle wholeRect;
	private Vector2 wholeRectOffset;
	private Vector2 wholeRectSize;
	private float rectOffsetX = 0.3f;
	private float rectOffsetY = 0.2f;
	private Vector2 position;
	boolean isStopped;

	private float interpolatedX, interpolatedY;
	private float dataX, dataY;

	SpineItem (Player.Side side) {
		this.side = side;

		currSpeed = new Vector2();
		currDirection = new Vector2();
		position = new Vector2();
	}

	void setName(String spineName) {
		createSpineItem(spineName);
	}

	void render(PolygonSpriteBatch batch, float delta) {
		interpolatedX = Interpolation.linear.apply(interpolatedX, dataX, 0.1f);
		interpolatedY = Interpolation.linear.apply(interpolatedY, dataY, 0.1f);

		if (isWalking) {
			int direction = side == Player.Side.left ? 1 : -1;
			skeleton.setX(skeleton.getX()+delta * direction);
		} else if (!isStopped) {
			if (!local) {
				skeleton.setPosition(interpolatedX, interpolatedY);
			} else {
				makeRandomMoves(delta);
			}
		}

		skeleton.update(delta);
		skeleton.updateWorldTransform();
		state.update(delta);
		state.apply(skeleton);

		skeleton.getBounds(wholeRectOffset, wholeRectSize);
		wholeRect.set(wholeRectOffset.x + rectOffsetX, wholeRectOffset.y + rectOffsetY,
			wholeRectSize.x - rectOffsetX * 2, wholeRectSize.y - rectOffsetY * 2);
		batch.begin();
		skeletonMeshRenderer.draw(batch, skeleton);
		batch.end();
	}

	private void makeRandomMoves (float delta) {
		if (tempTime < currTimeToChange) {
			skeleton.setX(skeleton.getX() + delta * currSpeed.x * currDirection.x);
			skeleton.setY(skeleton.getY() + delta * currSpeed.y * currDirection.y);
			tempTime += delta;
		} else {
			setNewWalkingParams();
		}
	}

	private void createSpineItem(String spineName) {
		TextureAtlas atlas = new TextureAtlas(Gdx.files.internal("characters/"+spineName+"/skeleton.atlas"));
		AtlasAttachmentLoader attachmentLoader = new AtlasAttachmentLoader(atlas);
		SkeletonJson json = new SkeletonJson(attachmentLoader);
		json.setScale(1/90f);
		SkeletonData skeletonData = json.readSkeletonData(Gdx.files.internal("characters/"+spineName+"/skeleton.json"));
		skeleton = new Skeleton(skeletonData);
		skeleton.setPosition(side == Player.Side.right? 10 : 0, 0.8f);
		skeleton.setFlipX(side == Player.Side.left);
		skeletonMeshRenderer = new SkeletonMeshRenderer();
		AnimationStateData stateData = new AnimationStateData(skeletonData);
		state = new AnimationState(stateData);
		state.setAnimation(0, "walk", true);
		state.addAnimation(0, "head kik", true, 0.6f);
//		state.setTimeScale(3.5f);

		Attachment hAttachment = skeleton.getAttachment("headp", "headp");
		headSlot = skeleton.findSlot("headp");
		headBoundingBox = (BoundingBoxAttachment) hAttachment;
		headWorldVertices = new float[headBoundingBox.getWorldVerticesLength()];

		Attachment wAttachment = skeleton.getAttachment("headphones", "headphones");
		weaponSlot = skeleton.findSlot("headphones");
//		weaponBoundingBox = (BoundingBoxAttachment) wAttachment;
//		weaponWorldVertices = new float[weaponBoundingBox.getWorldVerticesLength()];

		wholeRect = new Rectangle();
		wholeRectOffset = new Vector2();
		wholeRectSize = new Vector2();
		skeleton.updateWorldTransform();
		skeleton.getBounds(wholeRectOffset, wholeRectSize);
		wholeRect.width = wholeRectSize.x - rectOffsetX;
		wholeRect.height = wholeRectSize.y - rectOffsetY;
	}

	Polygon getHeadPolygon() {
		headBoundingBox.computeWorldVertices(headSlot, headWorldVertices);
		return new Polygon(headWorldVertices);
	}

	private int getDirection() {
		return directions[MathUtils.random(0, 1)];
	}

	private float getSpeed() {
		return MathUtils.random(1f, 3.5f);
	}

	private float getChangeTime() {
		return MathUtils.random(1f, 1f);
	}

	void setNewWalkingParams () {
		currTimeToChange = getChangeTime();
		tempTime = 0;
		setWalkingXParams();
		setWalkingYParams();
	}

	void startData() {
		interpolatedX = dataX = skeleton.getX();
		interpolatedY = dataY = skeleton.getY();
	}

	private void setWalkingXParams() {
		currDirection.x = getDirection();
		skeleton.setFlipX(currDirection.x == 1);
		currSpeed.x = getSpeed();
	}

	private void setWalkingYParams() {
		currDirection.y = getDirection();
		currSpeed.y = getSpeed();
	}

	Rectangle getWholeRect() {
		return wholeRect;
	}

	void changeDirectionX() {
		currDirection.x *= -1;
		skeleton.setFlipX(currDirection.x == 1);
	}

	void changeDirectionY() {
		currDirection.y *= -1;
	}

	void setPosition(float x, float y) {
		dataX = x;
		dataY = y;
	}

	Vector2 getPostion() {
		return position.set(skeleton.getX(), skeleton.getY());
	}

	void hitHead() {
//		state.addAnimation(0, "head kik", false, 0.6f);
//		state.addAnimation(0, "walk", true, 0);
	}

	void throwWeapon() {
		state.addAnimation(0, "kik", false, 0.6f);
	}

	void die() {
		state.setAnimation(0, "dead", false);
	}

	void win() {
		state.setAnimation(0, "happy", false);
	}

	Polygon getWeaponPolygon () {
		weaponBoundingBox.computeWorldVertices(weaponSlot, weaponWorldVertices);
		return new Polygon(weaponWorldVertices);
	}

	public boolean isLocal () {
		return local;
	}

	public void setLocal(boolean local) {
		this.local = local;
	}
}
