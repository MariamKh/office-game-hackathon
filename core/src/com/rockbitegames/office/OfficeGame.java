package com.rockbitegames.office;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.rockbitegames.office.menu.MenuScreen;

public class OfficeGame extends Game {

	public TextureAtlas textureAtlas;
	public String[] characters = new String[] {"avik", "gave", "edo", "hayk", "levon", "anahit"};
	public String localPlayerName;
	public String partnerPlayerName;
	private GameScreen gameScreen;
	public MenuScreen menuScreen;
	NetworkManager networkManager;
	ExtendViewport viewport;
	float viewportWidth;
	float maxViewportHeight;

	public OfficeGame () {

	}

	@Override
	public void create () {
		float aspect = 16/9f;

		//w/h

		viewportWidth = 10;
		maxViewportHeight = viewportWidth / aspect;

		viewport = new ExtendViewport(viewportWidth,  maxViewportHeight);
		viewport.update(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), true);

		textureAtlas = new TextureAtlas("ui/ui.atlas");

		menuScreen = new MenuScreen(this);
		setScreen(menuScreen);
	}

	public void createNetworkManager() {
		networkManager = new NetworkManager(new SocketListener() {
			@Override
			public void onInit(final int playerIndex) {
				Gdx.app.postRunnable(new Runnable() {
					@Override
					public void run() {
						menuScreen.selectBgMusic.stop();
						showGameScreen(playerIndex);
					}
				});
			}

			@Override
            public void onPartnerCoordsReceived(NetworkManager.PlayerData playerData) {
                gameScreen.setPartnerCoords(playerData.getX(), playerData.getY());
            }

			@Override
			public void onHit (NetworkManager.DamageData damageData) {
				gameScreen.hitHead(damageData.getDamage());
			}
		}, this);
	}

	public void showGameScreen(int playerIndex) {
		gameScreen = new GameScreen(this);
		gameScreen.onInit(playerIndex);
		setScreen(gameScreen);
	}

	GameScreen getGameScreen() {
		return gameScreen;
	}

	@Override
	public void render () {
		super.render();

		viewport.getCamera().update();
	}

	@Override
	public void resize (int width, int height) {
		super.resize(width, height);

		viewport.update(width, height);
		viewport.getCamera().position.set(5, viewport.getWorldHeight()/2f, 0);
	}

	@Override
	public void dispose () {
		super.dispose();

		textureAtlas.dispose();
	}

	public void setMenuScreen () {
		if (gameScreen != null) {
			gameScreen.dispose();
		}
		setScreen(menuScreen);
	}
}
