package com.rockbitegames.office;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.viewport.ExtendViewport;

public class GameScreen implements Screen {

	UIStage stage;
	 Player player1;
	 Player player2;
	Music battleBgMusic;

	private PolygonSpriteBatch batch;
	private OfficeGame game;

	private Texture background;
	private Rectangle fightRect;

	private ShapeRenderer shapeRenderer;

	GameScreen (OfficeGame game) {
		this.game = game;

		batch = new PolygonSpriteBatch();

		ExtendViewport extendViewport = new ExtendViewport(1920, 1080);

		stage = new UIStage(game, extendViewport);
		Gdx.input.setInputProcessor(stage);

		battleBgMusic = Gdx.audio.newMusic(Gdx.files.internal("music/battleBg.ogg"));

		background = new Texture(Gdx.files.internal("bg.png"));
		float x = 2f;
		float y = 0.7f;
		fightRect = new Rectangle(x, y, game.viewportWidth - x * 2, game.maxViewportHeight - y * 3);
		shapeRenderer = new ShapeRenderer();
		shapeRenderer.setAutoShapeType(true);
	}

	void onInit(int playerIndex) {
		player1 = new Player(Player.Side.left);
		player2 = new Player(Player.Side.right);

		switch (playerIndex) {
			case 1:
				player1.setLocal(true);
				player1.setName(game.localPlayerName);
				player2.setName(game.partnerPlayerName);
				break;
			case 2:
				player2.setLocal(true);
				player2.setName(game.localPlayerName);
				player1.setName(game.partnerPlayerName);
				break;
		}
		player1.startWalking();
		player2.startWalking();
	}

	void hitHead(float damage) {
		if (player1.isLocal()) {
			player1.loseHp(damage);
			stage.updateHpView(1, player1.getHpPartOfMax());
		} else if (player2.isLocal()) {
			player2.loseHp(damage);
			stage.updateHpView(2, player2.getHpPartOfMax());
		}
		stage.isSent = false;
	}

	void throwWeapon() {
		if (player1.isLocal()) {
			player1.throwWeapon();
		} else if (player2.isLocal()) {
			player2.throwWeapon();
		}
	}

	void win(Player player) {
		player.win();
	}

	void die(Player player) {
		player.die();
	}

	void setPartnerCoords(float x, float y) {
		if (player1.isLocal()) {
			player2.setPosition(x, y);
		} else if (player2.isLocal()) {
			player1.setPosition(x, y);
		}
	}

	@Override
	public void show () {
		battleBgMusic.play();
	}

	@Override
	public void render (float delta) {
		batch.setProjectionMatrix(game.viewport.getCamera().combined);

		batch.begin();

		Vector3 position = game.viewport.getCamera().position;
		float viewportWidth = game.viewport.getCamera().viewportWidth;
		float viewportHeight = game.viewport.getCamera().viewportHeight;

		batch.draw(background, position.x - viewportWidth/2f, position.y - viewportHeight/2f, viewportWidth, viewportHeight);
		batch.end();

//		shapeRenderer.setProjectionMatrix(game.viewport.getCamera().combined);
//		shapeRenderer.begin();
//		shapeRenderer.setColor(Color.BLUE);
//		shapeRenderer.rect(fightRect.x, fightRect.y, fightRect.width, fightRect.height);
//		shapeRenderer.end();

		player1.render(batch, delta);
		player2.render(batch, delta);
		checkIntersectionWithFightRect(player1);
		checkIntersectionWithFightRect(player2);
//		if (Intersector.overlapConvexPolygons(player1.getWeapon(), player2.getHead(), mvt)) {
//			player2.loseHp(5);
//			stage.updateHpView(2, player2.getHpPartOfMax());
//		}
//		if (Intersector.overlapConvexPolygons(player1.getHead(), player2.getWeapon(), mvt)) {
//			player1.loseHp(5);
//			stage.updateHpView(1, player1.getHpPartOfMax());
//		}

		stage.act(delta);
		stage.draw();
	}

	private void checkIntersectionWithFightRect (Player player) {
		if (fightRect.contains(player.getWholeRect())) {
			if (player.isWalking()) {
				player.stopWalking();
			} else {
				if (player1.isLocal()) {
					game.networkManager.playerCoords(player1.getPosition());
				} else if (player2.isLocal()) {
					game.networkManager.playerCoords(player2.getPosition());
				}
			}
		} else {
			if (!player.isWalking()) {
				if (player.getWholeRect().x < fightRect.x && player.getDirection().x == -1
					|| player.getWholeRect().x + player.getWholeRect().getWidth() > fightRect.x + fightRect.width && player.getDirection().x == 1) player.changeDirectionX();
				if (player.getWholeRect().y < fightRect.y && player.getDirection().y == -1
					|| player.getWholeRect().y + player.getWholeRect().getHeight() > fightRect.y + fightRect.height && player.getDirection().y == 1) player.changeDirectionY();
			}
		}
	}

	@Override
	public void resize (int width, int height) {
		stage.getViewport().update(width, height, true);
	}

	@Override
	public void pause () {

	}

	@Override
	public void resume () {

	}

	@Override
	public void hide () {

	}

	@Override
	public void dispose () {
		battleBgMusic.dispose();
		stage.dispose();
	}
}
