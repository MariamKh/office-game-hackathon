package com.rockbitegames.office;

import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

class Player {

	private SpineItem spineItem;
	private float maxHp = 1000;
	private float currHp = maxHp;

	public enum Side {
		left,
		right
	}

	Player (Side side) {
		spineItem = new SpineItem(side);
	}

	void setName(String name) {
		spineItem.setName(name);
	}

	void render(PolygonSpriteBatch batch, float delta) {
		spineItem.render(batch, delta);
	}

	void startWalking () {
		spineItem.isWalking = true;
	}

	void stopWalking () {
		spineItem.startData();
		spineItem.isWalking = false;
		spineItem.setNewWalkingParams();
	}

	boolean isWalking() {
		return spineItem.isWalking;
	}

	void changeDirectionX() {
		spineItem.changeDirectionX();
	}

	void changeDirectionY() {
		spineItem.changeDirectionY();
	}

	Vector2 getDirection() {
		return spineItem.currDirection;
	}

	Polygon getHead() {
		return spineItem.getHeadPolygon();
	}

	Polygon getWeapon() {
		return spineItem.getWeaponPolygon();
	}

	Rectangle getWholeRect() {
		return spineItem.getWholeRect();
	}

	void setPosition(float x, float y) {
		spineItem.setPosition(x, y);
	}

	Vector2 getPosition() {
		return spineItem.getPostion();
	}

	void throwWeapon() {
		if (!isWalking()) spineItem.throwWeapon();
	}

	void hitHead() {
		if (!isWalking()) spineItem.hitHead();
	}

	void win() {
		spineItem.win();
	}

	void die() {
		spineItem.die();
	}

	void loseHp(float size) {
		currHp-=size;
	}

	float getHpPartOfMax() {
		return currHp / maxHp;
	}

	public boolean isLocal () {
		return spineItem.isLocal();
	}

	public void setLocal (boolean local) {
		spineItem.setLocal(local);
	}

	float getHp() {
		return currHp;
	}

	void setStopped() {
		spineItem.isStopped = true;
	}
}
