package com.rockbitegames.office.menu;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.rockbitegames.office.OfficeGame;

public class MenuUIStage extends Stage {

	public MenuUIStage(final OfficeGame game, ExtendViewport viewport) {
		super(viewport);

		Vector2[] points = new Vector2[game.characters.length];
		points[0] = new Vector2(450, 720);
		points[1] = new Vector2(1200, points[0].y);
		points[2] = new Vector2(100, 390);
		points[3] = new Vector2(1560, points[2].y);
		points[4] = new Vector2(450, 70);
		points[5] = new Vector2(1200, points[4].y);

		for (int i=0; i<game.characters.length; i++) {
			final String name = game.characters[i];
			TextureRegion throwTexture = game.textureAtlas.findRegion(name);
			Image bubble = new Image(throwTexture);
			bubble.setScale(0.7f);
			bubble.setPosition(points[i].x, points[i].y);
			addActor(bubble);
			bubble.addListener(new ClickListener() {
				@Override
				public void clicked (InputEvent event, float x, float y) {
					super.clicked(event, x, y);

					game.localPlayerName = name;
					game.createNetworkManager();
				}
			});
		}

		TextureRegion throwTexture = game.textureAtlas.findRegion("shoos");
		Image chooseImg = new Image(throwTexture);
		chooseImg.setPosition(viewport.getWorldWidth()/2f - chooseImg.getWidth()/2f,viewport.getWorldHeight()/2f - chooseImg.getHeight()/2f);
		addActor(chooseImg);
	}

}
