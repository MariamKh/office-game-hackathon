package com.rockbitegames.office.menu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.rockbitegames.office.OfficeGame;

public class MenuScreen implements Screen {

	private MenuUIStage menuUIStage;
	public Music selectBgMusic;

	public MenuScreen (OfficeGame game) {
		ExtendViewport extendViewport = new ExtendViewport(1920, 1080);

		menuUIStage = new MenuUIStage(game, extendViewport);

		selectBgMusic = Gdx.audio.newMusic(Gdx.files.internal("music/selectBg.ogg"));
	}

	@Override
	public void show () {
		Gdx.input.setInputProcessor(menuUIStage);

		selectBgMusic.play();
	}

	@Override
	public void render (float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 0);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		menuUIStage.act(delta);
		menuUIStage.draw();
	}

	@Override
	public void resize (int width, int height) {
		menuUIStage.getViewport().update(width, height, true);
	}

	@Override
	public void pause () {

	}

	@Override
	public void resume () {

	}

	@Override
	public void hide () {

	}

	@Override
	public void dispose () {
		selectBgMusic.dispose();
		menuUIStage.dispose();
	}
}
