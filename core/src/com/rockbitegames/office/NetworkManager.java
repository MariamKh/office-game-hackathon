package com.rockbitegames.office;

import com.badlogic.gdx.math.Vector2;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;

public class NetworkManager {

    public static final String IP_ADDRESS = "http://206.189.226.108";
    private Socket socket;
    private  OfficeGame officeGame;

    public NetworkManager(SocketListener socketListener, OfficeGame officeGame) {
        this.officeGame = officeGame;
        createSocket(socketListener);
    }

    public String code;

    private void createSocket(final SocketListener socketListener) {
        try {
            String socketAddress = IP_ADDRESS + ":3000";
            socket = IO.socket(socketAddress);
            socket.open();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        socket.on("code", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                NetworkManager.this.code = args[0].toString();
//                System.out.println("code: " + code);
                match();
            }
        }).on("init", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                try {
                    JSONObject jsonObject = (JSONObject) args[0];
                    String room = jsonObject.getString("room");
                    JSONObject firstObj = jsonObject.getJSONObject("first");
                    JSONObject secondObj = jsonObject.getJSONObject("second");

                    String firstPlayerCode = firstObj.getString("code");
                    String firstPlayerName = firstObj.getString("name");

                    String secondPlayerCode = secondObj.getString("code");
                    String secondPlayerName = secondObj.getString("name");

                    if (firstPlayerCode.equals(NetworkManager.this.code)) {
                        socketListener.onInit(1);
                        officeGame.partnerPlayerName = secondPlayerName;
                    } else if (secondPlayerCode.equals(NetworkManager.this.code)) {
                        socketListener.onInit(2);
                        officeGame.partnerPlayerName = firstPlayerName;
                    }

                    NetworkManager.this.code = room;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).on("receivePartnerCoords", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                try {
                    PlayerData playerData = parseToObject(args);
                    socketListener.onPartnerCoordsReceived(playerData);
//                    System.out.println(" RECEIVE_PARTNER_COORDS" + "x:" + playerData.x + " y: " + playerData.y);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).on("hitReceived", new Emitter.Listener() {
            @Override
            public void call(Object... args) {

                try {
                    DamageData damageData = parseToDamage(args);
                    socketListener.onHit(damageData);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private final PlayerData playerData = new PlayerData();
    private final DamageData damageData = new DamageData();

    public void playerCoords(Vector2 position) {
        String playerJsonData = convertToJson(playerData.setData(position.x, position.y, code));
        socket.emit("partnerCoords", playerJsonData);
    }

    public void hit(float damage) {
        String playerJsonData = convertDamageDataToJson(damageData.setData(damage, code));
        socket.emit("hit", playerJsonData);
    }

    private final PlayerData playerReceivedData = new PlayerData();
    private final DamageData damageReceivedData = new DamageData();

    private PlayerData parseToObject(Object... objects) throws JSONException {
        JSONObject jsonObject = (JSONObject) objects[0];
        playerReceivedData.setX(Float.parseFloat(jsonObject.getString("x")));
        playerReceivedData.setY(Float.parseFloat(jsonObject.getString("y")));
        playerReceivedData.setCode(jsonObject.getString("room"));
        return playerReceivedData;
    }

    private DamageData parseToDamage(Object... objects) throws JSONException {
        JSONObject jsonObject = (JSONObject) objects[0];
        damageReceivedData.setDamage(Float.parseFloat(jsonObject.getString("damage")));
        damageReceivedData.setRoom(jsonObject.getString("room"));
        return damageReceivedData;
    }

    private String convertToJson(PlayerData playerData) {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("x", playerData.x);
            jsonObject.put("y", playerData.y);
            jsonObject.put("room", playerData.room);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }

    private String convertDamageDataToJson(DamageData damageData){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("damage", damageData.damage);
            jsonObject.put("room", damageData.room);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    public void friendlyMatch() {
        if (code != null) {
            socket.emit("friendlyMatch", code);
        }
    }

    public void match() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("playerName", officeGame.localPlayerName);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        socket.emit("match", jsonObject.toString());
    }

    class PlayerData {
        private float x;
        private float y;
        private String room;

        public void setX(float x) {
            this.x = x;
        }

        public void setY(float y) {
            this.y = y;
        }

        public void setCode(String room) {
            this.room = room;
        }

        public PlayerData setData(float x, float y, String room) {
            this.x = x;
            this.y = y;
            this.room = room;
            return this;
        }

        public float getX() {
            return x;
        }

        public float getY() {
            return y;
        }

        public String getRoom() {
            return room;
        }
    }

    class DamageData {
        private String room;
        private float damage;

        public DamageData () {
        }

        public DamageData setData(float damage, String room) {
            this.damage = damage;
            this.room = room;
            return this;
        }

        public DamageData (String room, float damage) {
            this.room = room;
            this.damage = damage;
        }

        public String getRoom () {
            return room;
        }

        public void setRoom (String room) {
            this.room = room;
        }

        public float getDamage () {
            return damage;
        }

        public void setDamage (float damage) {
            this.damage = damage;
        }
    }
}
