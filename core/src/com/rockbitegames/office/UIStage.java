package com.rockbitegames.office;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.viewport.Viewport;

class UIStage extends Stage {

	private OfficeGame game;
	private float width = 500f;
	private Label timerLabel;
	private int seconds = 30;
	private float tempTimePassed;
	private Image hp1;
	private Image hp2;
	private Label resultLbl;
	private Label newGameLbl;
	private Label yesLbl;
	private Label noLbl;
	private Timer timer;
	private Image throwBtn;
	private Image headHitBtn;
	private Intersector.MinimumTranslationVector mvt;
	boolean isSent;

	UIStage (final OfficeGame game, Viewport viewport) {
		super(viewport);
		this.game = game;

		timer = new Timer();

		TextureRegion throwTexture = game.textureAtlas.findRegion("ui-main-gear-btn");
		throwBtn = new Image(throwTexture);
		throwBtn.setPosition(10, 10);
		throwBtn.addListener(new ClickListener() {
			@Override
			public void clicked (InputEvent event, float x, float y) {
				super.clicked(event, x, y);

				game.getGameScreen().throwWeapon();
			}
		});
		addActor(throwBtn);

		mvt = new Intersector.MinimumTranslationVector();
		TextureRegion headHitTexture = game.textureAtlas.findRegion("ui-main-gear-btn");
		headHitBtn = new Image(headHitTexture);
		headHitBtn.setPosition( viewport.getWorldWidth() - headHitBtn.getWidth() - 10, 10);
		headHitBtn.addListener(new ClickListener() {
			@Override
			public void clicked (InputEvent event, float x, float y) {
				super.clicked(event, x, y);

				if (Intersector.overlapConvexPolygons(game.getGameScreen().player1.getHead(), game.getGameScreen().player2.getHead(), mvt)) {
					if (!isSent) {
						game.networkManager.hit(40);
						if (game.getGameScreen().player1.isLocal()) {
							game.getGameScreen().player2.loseHp(40);
							updateHpView(2, game.getGameScreen().player2.getHpPartOfMax());
						} else if (game.getGameScreen().player2.isLocal()) {
							game.getGameScreen().player1.loseHp(40);
							updateHpView(1, game.getGameScreen().player1.getHpPartOfMax());
						}
						isSent = true;
					}
				}
			}
		});
		addActor(headHitBtn);

		TextureRegion hp1Texture = game.textureAtlas.findRegion("ui-ingame-progress-fill-green");
		NinePatch ninePatch1 = new NinePatch(hp1Texture, 30, 30, 10, 10);
		NinePatchDrawable ninePatchDrawable1 = new NinePatchDrawable(ninePatch1);
		hp1 = new Image(ninePatchDrawable1);
		hp1.setPosition( 20, 1020);
		hp1.setWidth(width);
		addActor(hp1);

		TextureRegion hp2Texture = game.textureAtlas.findRegion("ui-ingame-progress-fill-green");
		NinePatch ninePatch2 = new NinePatch(hp2Texture, 30, 30, 10, 10);
		NinePatchDrawable ninePatchDrawable2 = new NinePatchDrawable(ninePatch2);
		hp2 = new Image(ninePatchDrawable2);
		hp2.setWidth(width);
		hp2.setPosition( viewport.getWorldWidth() - hp2.getWidth() - 20, 1020);
		addActor(hp2);

		Label.LabelStyle labelStyle = new Label.LabelStyle();
		labelStyle.font = new BitmapFont();
		timerLabel = new Label(getTimeNumericTextFromSeconds(seconds), labelStyle);
		timerLabel.setFontScale(3f);
		timerLabel.setX(viewport.getWorldWidth()/2f - timerLabel.getWidth()/2f);
		timerLabel.setY(viewport.getWorldHeight() - timerLabel.getHeight() - 30f);
		addActor(timerLabel);

		resultLbl = new Label("You won!", labelStyle);
		resultLbl.setFontScale(10f);
		resultLbl.setX(700);
		resultLbl.setY(800);
		resultLbl.setVisible(false);
		addActor(resultLbl);

		newGameLbl = new Label("New game?", labelStyle);
		newGameLbl.setFontScale(7f);
		newGameLbl.setX(730);
		newGameLbl.setY(500);
		newGameLbl.setVisible(false);
		addActor(newGameLbl);

		yesLbl = new Label("Yes!", labelStyle);
		yesLbl.setFontScale(10f);
		yesLbl.setScale(yesLbl.getFontScaleX(), yesLbl.getFontScaleY());
		yesLbl.setX(500);
		yesLbl.setY(200);
		yesLbl.setVisible(false);
		yesLbl.addListener(new ClickListener() {
			@Override
			public void clicked (InputEvent event, float x, float y) {
				super.clicked(event, x, y);

				game.setMenuScreen();
			}
		});
		addActor(yesLbl);

		noLbl = new Label("No.", labelStyle);
		noLbl.setFontScale(10f);
		noLbl.setScale(noLbl.getFontScaleX(), noLbl.getFontScaleY());
		noLbl.setX(1200);
		noLbl.setY(200);
		noLbl.setVisible(false);
		noLbl.addListener(new ClickListener() {
			@Override
			public void clicked (InputEvent event, float x, float y) {
				super.clicked(event, x, y);

				Gdx.app.exit();
			}
		});
		addActor(noLbl);
	}

	void updateHpView(int index, float part) {
		switch (index) {
		case 1:
			hp1.setWidth(width * part);
			break;
		case 2:
			hp2.setWidth(width * part);
			break;
		}
	}

	@Override
	public void act (float delta) {
		super.act(delta);

		timerLabel.setText(getTimeNumericTextFromSeconds(seconds));
		tempTimePassed+=delta;
		if (seconds > 0 && tempTimePassed >= 1) {
			seconds--;
			tempTimePassed = 0;
			if (seconds <= 0) {
				game.getGameScreen().battleBgMusic.stop();
				game.getGameScreen().player1.setStopped();
				game.getGameScreen().player2.setStopped();
				if (game.getGameScreen().player1.getHp() < game.getGameScreen().player2.getHp()) {
					game.getGameScreen().die(game.getGameScreen().player1);
					game.getGameScreen().win(game.getGameScreen().player2);
				} else {
					game.getGameScreen().die(game.getGameScreen().player2);
					game.getGameScreen().win(game.getGameScreen().player1);
				}
				headHitBtn.setTouchable(Touchable.disabled);
				throwBtn.setTouchable(Touchable.disabled);
				timer.scheduleTask(new Timer.Task() {
					@Override
					public void run () {
						resultLbl.setVisible(true);
						yesLbl.setVisible(true);
						noLbl.setVisible(true);
						newGameLbl.setVisible(true);
					}
				}, 1.5f);
			}
		}
	}

	@Override
	public void dispose () {
		super.dispose();

	}

	public static String getTimeNumericTextFromSeconds(int allSeconds) {
		String formatedString   =   "";
		int hours = allSeconds / 3600;
		int remainder = allSeconds - hours * 3600;
		int mins = remainder / 60;
		remainder = remainder - mins * 60;
		int secs = remainder;
		if(mins>0){
			if(mins<10){
				formatedString = formatedString + "0" + mins + ":";
			}else {
				formatedString = formatedString + mins + ":";
			}
		}else{
			formatedString = formatedString + "00:";
		}
		if(secs>=0){
			if(secs<10) {
				formatedString = formatedString + "0" + secs + "";
			}else{
				formatedString = formatedString + secs + "";
			}
		}else{
			formatedString = formatedString +  "00";
		}
		return formatedString;
	}
}
